
from django.contrib import admin
from statix.models.attachment import Attachment
from statix.models.image import Image
from statix.models.news import News
from django.contrib.admin import ModelAdmin
from statix.models.page import Page


class ImageInline(admin.TabularInline):

	fields = ('file', 'title', 'code_to_include',)
	readonly_fields = ('code_to_include',)
	model = Image


class AttachmentInline(admin.TabularInline):

	fields = ('file', 'name',)
	model = Attachment


class NewsAdmin(ModelAdmin):

	list_display = ('title', 'publish',)
	# inlines = [ImageInline, AttachmentInline]


"""
	regretably, this does not work without request, and getting request
	here is really much more work than it's worth
"""
#class PageForm(ModelForm):
#	model = Page
#	def clean_content(self, *args, **kwargs):
#		try:
#			Page.attempt_render(self.cleaned_data['content'])
#		except TemplateSyntaxError, err:
#			raise ValidationError('there is a problem with the template; it was prevented from saving: %s' % err)
#		return super(PageAdmin, self).clean_content(*args, **kwargs)


class PageAdmin(ModelAdmin):

	model = Page
#	form = PageForm
	fields = ('path', 'title', 'content')
	list_display = ('path', 'title',)
	inlines = [ImageInline, AttachmentInline]


admin.site.register(Page, PageAdmin)
admin.site.register(News, NewsAdmin)


