
from os.path import join
from settings import STATIX_URL, ROOT_URLCONF
from django.core.urlresolvers import LocaleRegexURLResolver
from django.utils.translation import get_language
from statix.models.page import Page


class StatixPageNotFound(Exception):
	pass


def lang_in_urls():
	"""

	"""
	if not hasattr(lang_in_urls, '_active'):
		urlsfile = __import__(ROOT_URLCONF)
		try:
			urlresolver = urlsfile.urlpatterns[0]
		except IndexError:
			urlresolver = None
		setattr(lang_in_urls, '_active', isinstance(urlresolver, LocaleRegexURLResolver))
	return getattr(lang_in_urls, '_active')


def statix_url(path, request = None):
	path = path.strip('/')
	try:
		page = Page.objects.get(path = path)
	except Page.DoesNotExist:
		raise StatixPageNotFound('tried to get url for static page %s, but it does not exist' % path)
	if request and lang_in_urls():
		path = join(get_language(), path)
	return join(STATIX_URL, path) + '/'


