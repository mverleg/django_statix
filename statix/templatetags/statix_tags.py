
from django import template
from displayable.templatetags.display import display
from statix.functions import statix_url as statix_url_func
from statix.models.image import Image


register = template.Library()


@register.simple_tag(takes_context = True)
def statix_url(context, path):
	"""
		returns the url to a statix page given path;
		this page isn't guaranteed to exist [for now]
	"""
	try:
		request = context['request']
	except KeyError:
		request = None
	return statix_url_func(path = path, request = request)


@register.simple_tag(takes_context = True)
def img(context, name, *flags, **styles):
	"""
		display an Image as html img tag
		flags: left, right, center, inline, small, medium, large
	"""
	try:
		img = Image.objects.get(name = name)
	except Image.DoesNotExist:
		raise Image.DoesNotExist('{%% img %%} tag from statix called with unknown image name \'%s\'' % name)
	if 'left' in flags:
		styles['float'] = 'left'
	if 'right' in flags:
		styles['float'] = 'right'
	if 'center' in flags:
		styles['float'] = 'center'
	if 'inline' in flags:
		styles['display'] = 'inline'
	if 'small' in flags:
		styles['max_width'] = '160px'
		styles['max_height'] = '120px'
	if 'medium' in flags:
		styles['max_width'] = '400px'
		styles['max_height'] = '300px'
	if 'large' in flags:
		styles['max_width'] = '800px'
		styles['max_height'] = '600px'
	return display(context, img, **styles)


@register.simple_tag(takes_context = True)
def if_url_statix(context, path, output = 'active'):
	"""
		statix equivalent to ext.if_url
	"""
	path = statix_url_func(path)
	if 'request' in context:
		if context['request'].path == path:
			return output
	return ''


