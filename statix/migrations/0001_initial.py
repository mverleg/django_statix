# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models, migrations
import statix.models.attachment
import statix.models.image
import django_resized.forms
import autoslug.fields
import django.core.validators
import displayable.classes


class Migration(migrations.Migration):

    dependencies = [
        ('reactables', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Attachment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('file', models.FileField(upload_to=statix.models.attachment.filename_to_name_field)),
                ('name', models.CharField(unique=True, blank=True, max_length=64, default=None)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(default=None, max_length=128, blank=True, null=True)),
                ('file', django_resized.forms.ResizedImageField(upload_to=statix.models.image.Image.filename_to_name_field_default)),
                ('name', models.CharField(unique=True, blank=True, max_length=64, default=None)),
            ],
            options={
            },
            bases=(models.Model, displayable.classes.DirectDisplayable),
        ),
        migrations.CreateModel(
            name='News',
            fields=[
                ('reactable_ptr', models.OneToOneField(auto_created=True, parent_link=True, serialize=False, primary_key=True, to='reactables.Reactable')),
                ('slug', autoslug.fields.AutoSlugField(editable=False)),
                ('title', models.CharField(max_length=32)),
                ('content', models.TextField(help_text='You can use html and django template language; take care not to create security vulnerabilities with the later', blank=True, default='')),
                ('publish', models.DateTimeField(help_text='The moment this news message is published (hidden until that)')),
            ],
            options={
                'verbose_name_plural': 'news',
                'ordering': ('-publish',),
            },
            bases=('reactables.reactable', displayable.classes.ChainDisplayable),
        ),
        migrations.CreateModel(
            name='Page',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('path', models.CharField(unique=True, help_text="the path of this page; do not include leadering and trailing /'s", db_index=True, max_length=32, validators=[django.core.validators.RegexValidator('^[a-z0-9][a-z0-9/_]+[a-z0-9]$', "only lower case lettesr, numbers and /'s are allowed")])),
                ('title', models.CharField(max_length=32)),
                ('content', models.TextField(help_text='You can use html and django template language; take care not to create security vulnerabilities with the later', blank=True, default='')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='image',
            name='target',
            field=models.ForeignKey(to='statix.Page', related_name='images'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='attachment',
            name='target',
            field=models.ForeignKey(to='statix.Page', related_name='attachments'),
            preserve_default=True,
        ),
    ]
