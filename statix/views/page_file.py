
from os.path import join
from django.http import Http404
from settings import DEBUG, MEDIA_ROOT
from statix.models.attachment import Attachment
from statix.models.image import Image
from django.views.static import serve


def page_file(request, Cls, key):
	"""
		find the file in the database if it exists, otherwise return a 404
		error; only to be used in debug mode
	"""
	if not DEBUG:
		raise Exception('statix files are only served in debug mode')
	try:
		inst = Cls.objects.get(key = key)
	except Cls.DoesNotExist:
		raise Http404()
	return serve(request, inst.file.url, document_root = join(MEDIA_ROOT, Cls.UPLOAD_DIR))


def page_image(request, key):
	return page_file(request, Image, key)


def page_attachment(request, key):
	return page_file(request, Attachment, key)


