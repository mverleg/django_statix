
from django.core.paginator import Paginator, EmptyPage
from django.shortcuts import render, redirect
from django.utils import timezone
from misc.decorators.instantiate import instantiate
from statix.models.news import News
from misc.views.notification import notification


def news_all(request, page = 1):
	"""
		all the news with pagination
	"""
	news = News.published.all()
	paginator = Paginator(news, 10)

	try:
		newsitems = paginator.page(int(page))
	except EmptyPage:
		newsitems = paginator.page(paginator.num_pages)

	return render(request, 'all_news.html', {
		'news': newsitems,
	})


def news_page(request, news, slug):
	"""
		full-page version news
	"""
	published = True
	if news.publish < timezone.now():
		if request.user.is_staff:
			published = False
		else:
			return notification(request, message = 'This news item is not yet available', subject = 'Not available yet')
	return render(request, 'news_page.html', {
		'news': news,
		'published': published,
	})


@instantiate(News)
def news_redirect(request, news):
	return redirect(to = news.get_absolute_url)


