
from django.http import Http404
from django.shortcuts import render
from statix.models.page import Page


def page(request, path):
	"""
		find the page in the database if it exists, otherwise return a 404
	"""
	try:
		page = Page.objects.get(path = path)
	except Page.DoesNotExist:
		raise Http404()
	return render(request, 'show_page.html', {
		'page': page,
		'content': page.rendered_content(request),
	})


