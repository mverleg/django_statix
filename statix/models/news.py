
from autoslug import AutoSlugField
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models.manager import Manager
from django.template.loader import render_to_string
from django.utils import timezone
from displayable import Displayable
from misc.functions.sanitize import sanitize_html
from reactables.models import Reactable
from misc.functions.render import render_str2str
from django.utils.safestring import mark_safe


class PublishedNewsManager(Manager):
	def get_queryset(self):
		return super(PublishedNewsManager, self).get_queryset().filter(publish__lt = timezone.now())


class News(Reactable, Displayable):

	allow_likes = True
	allow_dislikes = False
	allow_comments = False
	allow_ratings = False

	slug = AutoSlugField(populate_from = 'title')
	title = models.CharField(max_length = 32)
	content = models.TextField(default = '', blank = True, help_text = 'You can use html and django template language; take care not to create security vulnerabilities with the later')
	publish = models.DateTimeField(help_text = 'The moment this news message is published (hidden until that)')

	objects = Manager()
	published = PublishedNewsManager()

	class Meta:
		app_label = 'statix'
		ordering = ('-publish',)
		verbose_name_plural = 'news'

	def __unicode__(self):
		return self.slug

	def get_absolute_url(self):
		return reverse('news', kwargs = {'news_pk': self.pk, 'slug': self.slug})

	def display(self, context, request, **kwargs):
		return render_to_string('display_news.html', {
			'displayable': self,
			'content': self.rendered_content(request),
		}, context)

	def rendered_content(self, request):
		return mark_safe(sanitize_html(render_str2str(request, '{%% load statix_tags %%} %s' % self.content)))


