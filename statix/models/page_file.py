
from os.path import join
from django.db import models
from settings import STATIX_URL
from statix.models.page import Page


class PageFile(models.Model):
	
	target = models.ForeignKey(Page, related_name = '%(class)ss')
	
	class Meta:
		app_label = 'statix'
		abstract = True
	
	def get_absolute_url(self):
		return join(STATIX_URL, self.UPLOAD_DIR, self.file.url)


