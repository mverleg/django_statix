
from django.core.validators import RegexValidator
from django.db import models
from misc.functions.render import render_str2str
from django.template import Context, Template
from django.utils.safestring import mark_safe


class Page(models.Model):

	path = models.CharField(max_length = 32, unique = True, db_index = True, validators = [RegexValidator(r'^[a-z0-9][a-z0-9/_]+[a-z0-9]$', 'only lower case lettesr, numbers and /\'s are allowed')], help_text = 'the path of this page; do not include leadering and trailing /\'s')
	title = models.CharField(max_length = 32)
	content = models.TextField(default = '', blank = True, help_text = 'You can use html and django template language; take care not to create security vulnerabilities with the later')

	class Meta:
		app_label = 'statix'

	def __unicode__(self):
		return self.path

	def get_absolute_url(self):
		from statix.functions.statix_url import statix_url
		return statix_url(self.path)

	def rendered_content(self, request):
		return mark_safe(render_str2str(request, '{%% load statix_tags %%} %s<br class="clear" />' % self.content))

	''' attempt to render without request (because it's often not available) in order to detect template errors '''
	@staticmethod
	def attempt_render(content):
		return Template('{%% load statix_tags %%} %s<br class="clear" />' % content).render(Context())

