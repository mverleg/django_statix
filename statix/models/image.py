
from django.db import models
from django_resized import ResizedImageField
from misc.fields.filefield import filename_to_name_field
from displayable import DirectDisplayable
from statix.models.page_file import PageFile


class Image(PageFile, DirectDisplayable):

	UPLOAD_DIR = 'statix/img'

	def filename_to_name_field_default(instance, filename):
		return filename_to_name_field(instance, filename, upload_dir = Image.UPLOAD_DIR, name_field = 'name')

	title = models.CharField(max_length = 128, blank = True, null = True, default = None)
	file = ResizedImageField(max_width = 800, max_height = 800, upload_to = filename_to_name_field_default)
	name = models.CharField(max_length = 64, unique = True, blank = True, default = None)

	class Meta:
		app_label = 'statix'

	def __unicode__(self):
		return 'image %s' % self.name

	def code_to_include(self):
		return '{%% img \'%s\' %%}' % self.name

	def display(self, context, request, **styles):
		css = '; '.join('%s: %s' % (key, val) for key, val in styles.items())
		return '''<div class="statix_image" %(style)s>
			<img src="%(url)s" alt="image not loaded: \'%(title)s\'" title="%(title)s" />
		</div>''' % {
			'url': self.get_absolute_url(),
			'title': self.title or self.name,
			'style': ('style="%s;"' % css) if css else '',
		}
