
from django.db import models
from os.path import basename, splitext, join
from django.utils.text import slugify
from statix.models.page_file import PageFile


def filename_to_name_field(instance, filename):
	"""
		This gives a validation error if in one request (or simultaneous ones), two files
		with the same name are uploaded, because they'll generate the same .name field
		(both availabilities are checked before saving file or instance). However,
		that seems like a pretty rare and useless care; limit new file uploading if worried.
	"""
	barename, extension = splitext(basename(filename))
	path = join(instance.UPLOAD_DIR, '%s.%s' % (slugify(barename).replace('-', '_'), slugify(extension)))
	newpath = instance.file.storage.get_available_name(path)
	if not instance.name:
		instance.name = basename(newpath)
	return newpath


class Attachment(PageFile):

	UPLOAD_DIR = 'statix/attach'

	file = models.FileField(upload_to = filename_to_name_field)
	name = models.CharField(max_length = 64, unique = True, blank = True, default = None)

	class Meta:
		app_label = 'statix'

	def __unicode__(self):
		print(self.name)
		return 'attachment %s' % self.name


