
from django.conf.urls import patterns, url
from settings import STATIX_URL
from statix.models.attachment import Attachment
from statix.models.image import Image
from statix.views.news import news_page, news_redirect, news_all
from statix.views.page import page
from os.path import join
from statix.views.page_file import page_image, page_attachment


base_url = STATIX_URL.strip('/')
if base_url:
	base_url = '%s/' % base_url

urlpatterns = patterns('',
	url(r'^news/$', news_all, name = 'news_all'),
	url(r'^news/p(?P<page>[0-9]+)/$', news_all, name = 'news_all'),
	url(r'^news/~(?P<news_pk>[0-9]+)/$', news_redirect, name = 'news'),
	url(r'^news/~(?P<news_pk>[0-9]+)/(?P<slug>[^/]+)/$', news_page, name = 'news'),
	url(r'^%s/(?P<key>[a-z0-9\-_]+)/$' % join(base_url, Image.UPLOAD_DIR), page_image),
	url(r'^%s/(?P<key>[a-z0-9\-_]+)/$' % join(base_url, Attachment.UPLOAD_DIR), page_attachment),
    url(r'^%s(?P<path>[a-z0-9/_]+)/$' % base_url, page),
)


