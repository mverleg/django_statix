
Django Statix
-----------

A replacement for Django flat pages with some minor differences. It doesn't use the sites framework and it allows you to use images and attachments for pages. It also includes news, where you can add news articles instead of pages (also with images or attachments).

Installation & Configuration
-----------

Make sure you have Django Displayable and Django Reactable, the later are available here: https://bitbucket.org/mverleg/django_displayable.git and https://bitbucket.org/mverleg/django_reactable.git

- Install using ``pip install git+https://bitbucket.org/mverleg/django_statix.git`` (or download and copy the app into your project).
- Add ``reactables`` to ``INSTALLED_APPS``.
- Add ``url(r'^', include(statix.urls)),`` to THE END OF your urls.py.
- In ``settings.py``, add ``STATIX_URL`` as the base url for flat pages and news (e.g. ``STATIX_URL = '/'`` for no prefix)
- Run syncdb

Make sure MEDIA_ROOT is set correctly.

How to use
-----------

You can now add and change flat pages and news in the admin interface!

Future
-----------

- When a url is not found (not in urls.py and no statix page with that path), the 404 error doesn't show the patterns tried anymore.

License
-----------

django_statix is available under the revised BSD license, see LICENSE.txt. You can do anything as long as you include the license, don't use my name for promotion and are aware that there is no warranty.


